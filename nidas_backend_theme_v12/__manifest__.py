# -*- coding: utf-8 -*-
#
# Copyright (c) Openworx 2016, 2019.
# This file belongs to subproject NIDAS of project NIVA (www.niva4cap.eu)
# All rights reserved
#
# Project and code is made available under the EU-PL v 1.2 license.
#

{
    "name": "Nidas Backend Theme V12",
    "summary": "Nidas Backend Theme V12",
    "version": "12.0.0.1",
    "category": "Theme/Backend",
    "website": "http://www.openworx.nl",
	"description": """
		Openworx Material Backend theme for Odoo 12.0 community edition.
    """,
	'images':[
        'images/screen.png'
	],
    "author": "Openworx",
    "license": "LGPL-3",
    "installable": True,
    "depends": [
        'web',
        'web_responsive',

    ],
    "data": [
        'views/assets.xml',
		'views/res_company_view.xml',
		'views/users.xml',
        'views/sidebar.xml',
		#'views/web.xml',
    ],
    'live_test_url': 'https://youtu.be/JX-ntw2ORl8'

}

