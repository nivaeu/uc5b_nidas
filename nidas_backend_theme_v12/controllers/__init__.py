# -*- coding: utf-8 -*-
#
# Copyright (c) Openworx 2016, 2019.
# This file belongs to subproject NIDAS of project NIVA (www.niva4cap.eu)
# All rights reserved
#
# Project and code is made available under the EU-PL v 1.2 license.
#
from . import main