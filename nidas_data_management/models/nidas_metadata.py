#
# Copyright (c) Raffaele Amalfitano, Raffaele Del Gatto, Unitiva 2020 -- 2021.
# This file belongs to subproject NIDAS of project NIVA (www.niva4cap.eu)
# All rights reserved
#
# Project and code is made available under the EU-PL v 1.2 license.
#
from odoo import api, exceptions, fields, models

# **************************** NIDAS METADATA *******************************
class NidasMetadata(models.Model):
    """
    Nidas metadata
    """
    _name = 'nidas.metadata'
    _description = 'Nidas Metadata'

    name = fields.Char(string="Name", required=True)
    metadata_weight = fields.Float(string="Weight", default=1, required=False)
    metadata_type = fields.Selection([
        ('procedure', 'Procedure'),
        ('owner', 'Owner'),
        ('source', 'Source'),
        ('certification_level', 'Certification level'),
        ('genuine_farmer', 'Genuine Farmer validated by the farmer registry'),
        ('area_validated', 'Total area validated by the farmer registry'),
        ('area_found', 'Total area found validated by the monitoring system and LPIS'),
        ('02_ha_land', 'Total area payable for BISS least 0.2 ha of land'),
        ('payment_rights', 'You have corresponding payment rights'),
        ('GAEC_9', 'GAEC 9 - Maintenance of non-productive features and areas'),
        ('GAEC_10', 'GAEC 10 - Ban on converting or ploughing permanent grassland'),
        ('biodiversity', 'Biodiversity')
        ], string="Metadata type", readonly=True)

    @api.multi
    @api.constrains('name')
    def _check_metadata_name_unique(self):
        """
        Used to check metadata name.
        Name must be unique
        """
        for record in self:
            if len(self.search([('name','=',record.name), ('metadata_type','=',record.metadata_type), ('name', '!=', False)])) > 1:
                raise exceptions.ValidationError("Metadata '{}' already exists!".format(record.name))