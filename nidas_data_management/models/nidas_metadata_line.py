#
# Copyright (c) Raffaele Amalfitano, Raffaele Del Gatto, Unitiva 2020 -- 2021.
# This file belongs to subproject NIDAS of project NIVA (www.niva4cap.eu)
# All rights reserved
#
# Project and code is made available under the EU-PL v 1.2 license.
#
from odoo import api, fields, models

# **************************** NIDAS METADATA LINE *******************************
class NidasMetadataLine(models.Model):
    """
    Nidas metadata line
    """
    _name = 'nidas.metadata.line'
    _description = 'Nidas Metadata Line'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _rec_name = 'metadata_id'

    # foreign key to the 'nidas.data' model
    # remove childs when data_id object is removed
    data_id = fields.Many2one('nidas.data',
        string="Data", ondelete="cascade")

    # used to filter 'metadata_id' field
    metadata_type = fields.Selection([
        ('procedure', 'Procedure'),
        ('owner', 'Owner'),
        ('source', 'Source'),
        ('certification_level', 'Certification level'),
        ('genuine_farmer', 'Genuine Farmer validated by the farmer registry'),
        ('area_validated', 'Total area validated by the farmer registry'),
        ('area_found', 'Total area found validated by the monitoring system and LPIS'),
        ('02_ha_land', 'Total area payable for BISS least 0.2 ha of land'),
        ('payment_rights', 'You have corresponding payment rights'),
        ('GAEC_9', 'GAEC 9 - Maintenance of non-productive features and areas'),
        ('GAEC_10', 'GAEC 10 - Ban on converting or ploughing permanent grassland'),
        ('biodiversity', 'Biodiversity')
        ], string="Metadata type")

    # display only metadata related to the specified 'metadata_type' field
    metadata_id = fields.Many2one('nidas.metadata', string="Metadata",
        domain="[('metadata_type', '=', metadata_type)]",
        track_visibility=True)

    metadata_weight_rel = fields.Float(related='metadata_id.metadata_weight',
        string="Weight")
    