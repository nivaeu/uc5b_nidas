#
# Copyright (c) Raffaele Amalfitano, Raffaele Del Gatto, Unitiva 2020 -- 2021.
# This file belongs to subproject NIDAS of project NIVA (www.niva4cap.eu)
# All rights reserved
#
# Project and code is made available under the EU-PL v 1.2 license.
#
from odoo import api, fields, models

# **************************** NIDAS DATA *******************************
class NidasData(models.Model):
    """
    Nidas data
    """
    _name = 'nidas.data'
    _description = 'Nidas Data'
    _inherit = ['mail.thread', 'mail.activity.mixin']

    name = fields.Char(string="CUAA", required=True,
        track_visibility=True)
    transmission_date = fields.Date(string="Date",
        default=lambda self: fields.Date.today(),
        required=True, track_visibility=True)

    # def default_metadata_line_ids(self):
    #     """
    #     Used to fill 'metadata_line_ids' field
    #     with nidas metadata type
    #     """
    #     metadata_line_list = self.env['nidas.metadata.line']
    #     nidas_metadata = self.env['nidas.metadata']
    #     metadata_type_list = list(dict(nidas_metadata._fields['metadata_type'].selection).keys())
    #     for key in metadata_type_list:
    #         metadata_line_list |= metadata_line_list.create({'metadata_type': key})
    #     return metadata_line_list

    # # link to the 'nidas.metadata.line' model
    # metadata_line_ids = fields.One2many('nidas.metadata.line',
    #     'data_id', string="Metadata line", default=default_metadata_line_ids)
    metadata_line_ids = fields.One2many('nidas.metadata.line',
        'data_id', string="Metadata line")

    quality_index = fields.Float(string="Quality index", compute='_compute_quality_index',
        store=True, track_visibility=True)

    # used to set nidas data display name
    @api.multi
    def name_get(self):
        result = []
        for data in self:
            name_to_display = '{} - {}'.format(data.name, (data.transmission_date).strftime("%d/%m/%Y"))
            result.append((data.id, name_to_display))
        return result

    @api.multi
    @api.depends('metadata_line_ids')
    def _compute_quality_index(self):
        """
        Compute quality index multiplying weights
        of each specified metadata
        """
        for data in self:
            if data.metadata_line_ids:
                weight_list = data.metadata_line_ids.mapped(lambda x: x.metadata_weight_rel)
                data.quality_index = sum(weight_list)/len(weight_list)
            # if data.metadata_line_ids:
            #     prod = 1
            #     for m_line in data.metadata_line_ids:
            #         prod = prod * m_line.metadata_weight_rel
            #     data.quality_index = prod
            # else:        
            #     data.quality_index = 0
