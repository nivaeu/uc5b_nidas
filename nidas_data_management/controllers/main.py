#
# Copyright (c) Raffaele Amalfitano, Raffaele Del Gatto, Unitiva 2020 -- 2021.
# This file belongs to subproject NIDAS of project NIVA (www.niva4cap.eu)
# All rights reserved
#
# Project and code is made available under the EU-PL v 1.2 license.
#
from datetime import datetime
from odoo import http
from odoo.http import request


class NidasController(http.Controller):

    def _check_date(self, date):
        """
        Check if 'date' parameter is valid
        """
        format = "%d/%m/%Y"
        is_valid_date = True
        try:
            datetime.strptime(date, format)
            day, month, year = date.split('/')
            datetime(int(year),int(month),int(day))
        except ValueError:
            is_valid_date = False
        return is_valid_date

    def _check_metadata(self, value, metadata_type):
        """
        Check metadata existence
        params:
        - value: metadata value
        - metadata_type: type of metadata
        return:
        - metadata object
        """
        metadata_obj = request.env['nidas.metadata'].sudo().search([
            ('name', '=', value),
            ('metadata_type', '=', metadata_type),
        ], limit=1)
        if not metadata_obj:
            metadata_obj = metadata_obj.sudo().create({
                'name': value,
                'metadata_type': metadata_type
                })
        return metadata_obj

    def _add_metadata_line(self, metadata_line_list, metadata_type, metadata_obj):
        """
        Add metadata line to metadata list
        params:
        - metadata_line_list: metadata list
        - metadata_type: type of metadata
        - metadata_obj: metadata object
        """
        metadata_line_list.append(
            (0, 0, {
                'metadata_type': metadata_type,
                'metadata_id': metadata_obj.id
            })
        )

    def _check_int(self, s):
        try: 
            int(s)
            return True
        except ValueError:
            return False


    @http.route('/api/v1/data', type='json', methods=['POST','GET'], auth='none')
    def data_management(self, **kw):
        # POST REQUEST: DatasetRegistration() method reported on the document 
        if request.httprequest.method == 'POST':
            json_req = request.jsonrequest
            if json_req:
                if 'params' in json_req:
                    params = json_req['params']
                    if 'cuaa' in params and 'date' in params\
                    and params['cuaa'] != '' and params['date'] != '':
                        data_vals = {}
                        metadata_line_list = []
                        data_vals.update(name=params['cuaa'])
                        is_valid_date = self._check_date(params['date'])
                        if is_valid_date:
                            data_vals.update(transmission_date=datetime.strptime(params['date'], '%d/%m/%Y'))
                        else:
                            args = {
                                'success': False,
                                'status': 400, # Bad Request
                                'message': 'Invalid date!'
                            }
                            return args

                        # check procedures
                        if params['procedures']:
                            for procedure in params['procedures']:
                                metadata_obj = self._check_metadata(procedure['value'], 'procedure')
                                self._add_metadata_line(metadata_line_list, 'procedure', metadata_obj)

                        # check owners
                        if params['owners']:
                            for owner in params['owners']:
                                metadata_obj = self._check_metadata(owner['value'], 'owner')
                                self._add_metadata_line(metadata_line_list, 'owner', metadata_obj)

                        # check sources
                        if params['sources']:
                            for source in params['sources']:
                                metadata_obj = self._check_metadata(source['value'], 'source')
                                self._add_metadata_line(metadata_line_list, 'source', metadata_obj)

                        # check certification_levels
                        if params['certification_levels']:
                            for cert_level in params['certification_levels']:
                                metadata_obj = self._check_metadata(cert_level['value'], 'certification_level')
                                self._add_metadata_line(metadata_line_list, 'certification_level', metadata_obj)

                        # check genuine_farmer
                        if params['genuine_farmer']:
                            for genuine_f in params['genuine_farmer']:
                                metadata_obj = self._check_metadata(genuine_f['value'], 'genuine_farmer')
                                self._add_metadata_line(metadata_line_list, 'genuine_farmer', metadata_obj)

                        # check total area validated
                        if params['area_validated']:
                            for area_validated in params['area_validated']:
                                metadata_obj = self._check_metadata(area_validated['value'], 'area_validated')
                                self._add_metadata_line(metadata_line_list, 'area_validated', metadata_obj)
                        
                        # check total area found
                        if params['area_found']:
                            for area_found in params['area_found']:
                                metadata_obj = self._check_metadata(area_found['value'], 'area_found')
                                self._add_metadata_line(metadata_line_list, 'area_found', metadata_obj)

                        # check 02_ha_of_land
                        if params['02_ha_of_land']:
                            for ha_land in params['02_ha_of_land']:
                                metadata_obj = self._check_metadata(ha_land['value'], '02_ha_land')
                                self._add_metadata_line(metadata_line_list, '02_ha_land', metadata_obj)

                        # check payment_rights
                        if params['payment_rights']:
                            for payment in params['payment_rights']:
                                metadata_obj = self._check_metadata(payment['value'], 'payment_rights')
                                self._add_metadata_line(metadata_line_list, 'payment_rights', metadata_obj)

                        # check gaec_9
                        if params['gaec_9']:
                            for gaec_9 in params['gaec_9']:
                                metadata_obj = self._check_metadata(gaec_9['value'], 'GAEC_9')
                                self._add_metadata_line(metadata_line_list, 'GAEC_9', metadata_obj)

                        # check gaec_10
                        if params['gaec_10']:
                            for gaec_10 in params['gaec_10']:
                                metadata_obj = self._check_metadata(gaec_10['value'], 'GAEC_10')
                                self._add_metadata_line(metadata_line_list, 'GAEC_10', metadata_obj)

                        # check biodiversity
                        if params['biodiversity']:
                            for biodiversity in params['biodiversity']:
                                metadata_obj = self._check_metadata(biodiversity['value'], 'biodiversity')
                                self._add_metadata_line(metadata_line_list, 'biodiversity', metadata_obj)

                        # add metadata line list to data_vals
                        if metadata_line_list:
                            data_vals.update(metadata_line_ids=metadata_line_list)
                        nidas_data = request.env['nidas.data'].sudo().create(data_vals)
                        args = {
                            'success': True,
                            'status': 200, # OK, risorsa creata
                            'message': 'Data registered successfully!',
                            'data_id': nidas_data.id
                        }
                    else:
                        args = {
                            'success': False,
                            'status': 422, # Unprocessable Entity
                            'message': 'Missing mandatory data!'
                        }
                else:
                    args = {
                        'success': False,
                        'status': 422, # Unprocessable Entity
                        'message': 'Missing mandatory data!'
                    }
            return args

        # GET REQUEST: RequestQualityAtDate() method reported on the document
        elif request.httprequest.method == 'GET':
            query_params_str = request.httprequest.query_string.decode(encoding='UTF-8')
            if 'cuaa' in query_params_str and 'date' in query_params_str:
                query_list = query_params_str.split('&')
                # 'cuaa' sent as the first parameter
                if 'cuaa' in query_list[0]:
                    cuaa = query_list[0].split('=')[1]
                    date = query_list[1].split('=')[1]
                # 'cuaa' sent as the second parameter
                else:
                    cuaa = query_list[1].split('=')[1]
                    date = query_list[0].split('=')[1]
                is_valid_date = self._check_date(date)
                if is_valid_date:
                    date = datetime.strptime(date, '%d/%m/%Y')
                else:
                    args = {
                        'success': False,
                        'status': 400, # Bad Request
                        'message': 'Invalid date!'
                    }
                    return args
                nidas_data = request.env['nidas.data'].sudo().search([
                    ('name', '=', cuaa),
                    ('transmission_date', '<=', date)
                ], limit=1, order='transmission_date desc, create_date desc')
                if nidas_data:
                    args = {
                        'success': True,
                        'status': 200, # OK
                        'message': 'Quality index returned successfully!',
                        'data_id': nidas_data.id,
                        'quality_index': round(nidas_data.quality_index, 2)
                    }
                else:
                    args = {
                        'success': False,
                        'status': 404, # The requested resource was not found
                        'message': "No resources found with CUAA '{}' on the specified date!".format(cuaa)
                    }
            else:
                args = {
                    'success': False,
                    'status': 422, # Unprocessable Entity
                    'message': 'Missing mandatory data!'
                }
            return args


    @http.route('/api/v1/qindexes', type='json', methods=['GET'], auth='none')
    def get_index(self, **kw):
        # GET REQUEST: Get quality index of specific resource
        if request.httprequest.method == 'GET':
            data_id_str = request.httprequest.query_string.decode(encoding='UTF-8')
            if 'data_id' in data_id_str:
                data_id = data_id_str.split('=')[1]
                if self._check_int(data_id):
                    nidas_data = request.env['nidas.data'].sudo().search([('id', '=', int(data_id))])
                    if nidas_data:
                        args = {
                            'success': True,
                            'status': 200, # OK
                            'message': 'Quality index returned successfully!',
                            'data_id': int(data_id),
                            'quality_index': round(nidas_data.quality_index, 2)
                        }
                    else:
                        args = {
                            'success': False,
                            'status': 404, # The requested resource was not found
                            'message': "No resource found with 'data_id' = {}".format(data_id)
                        }
                else:
                    args = {
                        'success': False,
                        'status': 422, # Unprocessable Entity
                        'message': "Incorrect 'data_id' format"
                    }
            else:
                args = {
                    'success': False,
                    'status': 422, # Unprocessable Entity
                    'message': "Missing mandatory data! No 'data_id' specified!"
                }
            return args
            