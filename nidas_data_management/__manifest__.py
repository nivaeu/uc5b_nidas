#
# Copyright (c) Raffaele Amalfitano, Raffaele Del Gatto, Unitiva 2020 -- 2021.
# This file belongs to subproject NIDAS of project NIVA (www.niva4cap.eu)
# All rights reserved
#
# Project and code is made available under the EU-PL v 1.2 license.
#
{
	'name': "Nidas Data Management",
	'license': 'AGPL-3',
	'author': "Raffaele Amalfitano, Raffaele Del Gatto, Unitiva",
	'website': 'www.unitiva.it',
	'category': '',
	'version': '1.3.0',
	'depends': [
		'mail',
	],
	'data': [
		'security/ir.model.access.csv',
		'data/nidas_metadata.xml',
		'views/nidas_data_views.xml',
		'views/nidas_metadata_views.xml',
		'views/nidas_metadata_line_views.xml',
		'views/menu_views.xml',
	],
}
