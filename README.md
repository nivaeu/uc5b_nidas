# Introduction

This sub-project is part of the ["New IACS Vision in Action” --- NIVA](https://www.niva4cap.eu/) project that delivers a a suite of digital solutions, e-tools and good practices for e-governance and initiates an innovation ecosystem to support further development of IACS that will facilitate data and information flows.

This project has received funding from the European Union’s Horizon 2020 research and innovation programme under grant agreement No 842009.

Please visit the [website](https://www.niva4cap.eu) for further information. A complete list of the sub-projects made available under the NIVA project can be found on [gitlab](https://gitlab.com/nivaeu/)


# Features description

The main features of the NIDAS custom module, made for the specific purpose of handling the dataset, along with their relative metadata, coming from the Click&Pay platform for calculation of Quality Index, are illustrated below.
To access the app, the user needs to click on the icon at the top left corner, and then on the Nidas app icon (see the screenshot below).

![plot](./nidas_data_management/static/img/nidas_app.png)

This is what the user will see immediately, once he enters in the Nidas app:

![plot](./nidas_data_management/static/img/nidas_app_1.png)

As a reader can see, the system shows a list of all dataset loaded on the system and the quality index related.
At the top, instead, there are the following menu options: **Data**, **Metadata lines**, **Configuration**. In the following section will be explained how to configure the app, since it is recommended to have a set of ready to use options before starting.


## How configure the Nidas app

Through the configuration menu is possible to create, edit and delete:
- **Metadata options**

Within the Metadata/Configuration section there are the following options: **Procedures**, **Owners**, **Sources**, **Certification levels**, **Genuine Farmer validated by the farmer registry**, **Total area validated by the farmer registry**, **Total area found validated by the monitoring system and LPIS**, **Total area payable for BISS least 0.2 ha of land**, **You have corresponding payment rights**, **GAEC 9 - Maintenance of non-productive features and areas**, **GAEC 10 - Ban on converting or ploughing permanent grassland**, **Biodiversity**, **All metadata**.

![plot](./nidas_data_management/static/img/nidas_configuration.png)

In the following section will be explained how to configure the Metadata options.


### How configure the metadata options

The configuration of each metadata option is straightforward: for each voice all the user has to do is to assign a name to the option, with its relative weight. To make it more clear we are gonna create a procedure as an example. First of all, we have to click on the voice 'Procedures' under the Configuration menu. As usual, the system will show a list of existing procedures, which is possible to edit or delete. It is also possible to filter or to group the procedures by name or weight (the filtering and grouping features are explained in a specific section).

![plot](./nidas_data_management/static/img/nidas_procedures_1.png)

To edit a procedure the user has to click on it: the form view will open and it would be possible to change the name and weight parameters. In case it is needed to delete one or more procedures the user has to flag them, click on the **Action** menu and then on the **Delete** option.

![plot](./nidas_data_management/static/img/nidas_procedures_2.png)

Instead, to add a new procedure just click on the **Create** button and edit the form as indicated in the below image:

![plot](./nidas_data_management/static/img/nidas_procedures_3.png)

As we can see it is sufficient just to add a name and a weight: this latter parameter accepts only numeric values. Once those parameters have been inserted the user can finally click on the **Save** button (after this, the new procedure will be present in the system, ready to be used in a contract). The user may also notice the presence of the **Metadata type** field, which is present in any metadata form and it shows automatically the metadata that the user is about to add (for example, if the user will click on the procedure option, the field will be threaded as procedure).
The creation methodology of other metadata options is identical to the one seen above for the procedure. A little note of interest for the configuration voice 'All metadata': here are present all existing metadata options, despite the typology, in readonly form.


## Access important information quickly: Data and Metadata line sections

In the main menu, at the top, there are the options **Data** and **Metadata line**: both can be used to access important contract information quickly. 
If the user clicks on **Data** a page with information related to the cuaa, the date, the creation date, and the quality index will open (have a look at the image below).

![plot](./nidas_data_management/static/img/nidas_data_1.png)

At the other side, within the **Metadata lines** section, the info is aggregated by data (as is shown below).

![plot](./nidas_data_management/static/img/nidas_metadata.png)


## How to add, get and manage Data

It is possible to insert new Data manually, directly through the ERP system, or via API. In this section will be explained how a user, with access to the system, can add, edit and manage Data info, while interaction via API is explained in a dedicated section.
In order to add a new Data a user has to click on the **Data** menu, and then on the **Create** button. At this point the user will need to compile the data form view as follow:
1. Add the **CUAA**;
2. Add a **date**;
3. Add the **Metadata lines**, by selecting the **type** with its respective **weight**.

![plot](./nidas_data_management/static/img/nidas_data_create.png)

When the Medatada lines are added, at the end of the form is automatically calculated the **Quality Index** (which is calculated as arithmetic mean of all the metadata type weight).

![plot](./nidas_data_management/static/img/nidas_quality_index.png)

In the **Other info** tab there are fields such as **Created on**, **Last updated on**, **Last updated by** which will be automatically uploaded once the form is saved (see the image below).

![plot](./nidas_data_management/static/img/nidas_other_info.png)


### How add and get Data info via API

In this section we are gonna explain how to use the API in order to upload and retrieve Data info from the ERP system, through the two **endpoints** designed. 

**The first one**, http://yourdomain/api/v1/data (with 'yourdomain' segment which should accordingly be adjusted to the ERP system's real domain), accepts both **POST** and **GET** methods.


#### POST DATA - EXAMPLE

Here we are gonna show how to post a specific CUAA, with certain metadata parameters. 

```
Header: Content-Type: application/json
Body:
{
   "params": {
       "cuaa": "17686",
       "date": "22/02/2021",
       "procedures": [
           {
               "value": "procedure1_description"
           },
           {
        ...
        }
       ],
       "owners": [
           {
               "value": "AGEA"
           },
           {
        ...
        }
       ],
       "sources": [
           {
               "value": "Area monitoring"
           },
        {
        ...
        }
       ],
       "certification_levels": [
           {
               "value": "Internal"
           },
        {
        ...
        }
       ],
       "genuine_farmer": [
           {
               "value": "Yes"
           },
        {
        ...
        }
       ],
       "area_validated": [
           {
               "value": "Yes"
           },
        {
        ...
        }
       ],
       "area_found": [
           {
               "value": "No"
           },
        {
        ...
        }
       ],
       "02_ha_of_land": [
           {
               "value": "Yes"
           },
           {
        ...
        }
       ],
       "payment_rights": [
           {
               "value": "No"
           },
           {
        ...
        }
       ],
    "gaec_9": [
           {
               "value": "Yes"
           },
           {
        ...
        }
       ],
    "gaec_10": [
           {
               "value": "Yes"
           },
           {
        ...
        }
       ]
   }
}
```

**CUAA and date are mandatory fields**: if they are not specified or are inserted in the wrong way the system will reply with a bad response (the date field will only accept a date in the format dd/mm/yyyy).

This is how it looks a **POST** made through a curl request:

```
curl -X POST -H "Content-Type: application/json" http://yourdomain/api/v1/data -d '{"params": {"cuaa": "112255", "date": "05/10/2021", "procedures": [{"value": "procedure1_description"}, {"value": "procedure2_description"}], "owners": [{"value": "AGEA"}, {"value": "owner2_description"}], "sources": [{"value": "Area monitoring"}], "certification_levels": [{"value": "Internal"}], "genuine_farmer": [{"value": "Yes"}], "area_validated": [{"value": "Yes"}], "area_found": [{"value": "No"}], "02_ha_of_land": [{"value": "Yes"}], "payment_rights": [{"value": "No"}], "gaec_9": [{"value": "Yes"}], "gaec_10": [{"value": "Yes"}]}}'
```

If everything goes fine the API will return the id ("data_id" information) of the created data:

```
Response:
{
   "jsonrpc": "2.0",
   "id": null,
   "result": {
       "success": true,
       "status": 200,
       "message": "Data registered successfully!",
       "data_id": 7
   }
}
```

At this point, once the post is done, on the system will be created a new Data record with CUAA, Date and all the indicated metadata:

![plot](./nidas_data_management/static/img/nidas_api_data.png)

#### GET QUALITY INDEX AT DATE - EXAMPLE

Through the GET method it is possible to retrieve a CUAA quality index up to a certain date. If for example a user want to know the quality index of the CUAA 112255 at the most recent date up to 05/10/2021, those are the parameters that need to be specified:

```
Header: Content-Type: application/json
Query Params: KEY: cuaa, VALUE: 112255; KEY: date, VALUE: 05/10/2021
Body:
{
   "params": {
   }
}
```

And this is the curl request example:

```
curl -X GET -H "Content-Type: application/json" http://yourdomain/api/v1/data?cuaa=112255&date=05/10/2021 -d '{"params": {}}'
```

**CUAA and date are mandatory fields**: if they are not specified or are inserted in the wrong way the system will reply with a bad response (the date field will only accept a date in the format dd/mm/yyyy).

If the response is positive the system will return the id of the data, along with the quality index (which is a number contained in the range 0 - 1).

```
Response:
{
   "jsonrpc": "2.0",
   "id": null,
   "result": {
        "success": true,
        "status": 200,
        "message": "Quality index returned successfully!",
    "data_id": 7,
        "quality_index": 0.85
   }
}
```

**The second one**, http://yourdomain/api/v1/qindexes (with 'yourdomain' segment which should accordingly be adjusted to the ERP system's real domain), accepts **GET** method and returns the quality index of a specific 'data_id' managed as query parameter.


#### GET QUALITY INDEX - EXAMPLE

Through the GET method it is possible to retrieve a quality index of a specific data. If for example a user want to know the quality index of the data with 'data_id' = 18, those are the parameters that need to be passed:

```
Header: Content-Type: application/json
Query Params: KEY: data_id, VALUE: 18
Body:
{
   "params": {
   }
}
```
 
And this is the curl request example:

```
curl -X GET -H "Content-Type: application/json" http://yourdomain/api/v1/qindexes?data_id=18 -d '{"params": {}}'
```

**Query parameter 'data_id' is a mandatory field**: if it is not specified or the ID does not exist, the system will reply with a bad response.
If the response is positive the system will return the id of the data, along with the quality index (which is a number contained in the range 0 - 1).

```
Response:
{
   "jsonrpc": "2.0",
   "id": null,
   "result": {
        "success": true,
        "status": 200,
        "message": "Quality index returned successfully!",
    "data_id": 18,
        "quality_index": 0.64
   }
}
```


# Credits

## Authors

* Raffaele Amalfitano, Raffaele Del Gatto, Unitiva